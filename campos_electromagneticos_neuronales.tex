\documentclass[12pt, a4paper, oneside]{article} %Default is 10 pt, other available options are 11 pt and 12 pt.
%Preamble-----------------------------------------------------

%Spanish language------------
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}

\newcommand{\varTitleDocument}{Campos electromagnéticos neuronales}
\newcommand{\varAuthor}{Mario Michiels}

\usepackage{graphicx}
\graphicspath{ {images/} }
\usepackage{float}

\usepackage[x11names]{xcolor}

\usepackage[nottoc,notlot,notlof]{tocbibind}

\usepackage{helvet} %Helvet is an analogue to Arial 
\renewcommand{\familydefault}{\sfdefault}



\usepackage[a4paper, total={6in, 8in}]{geometry}

%\usepackage[
%	backend=bibtex,
%	style=alphabetic, %Reference style
%]{biblatex}
%\addbibresource{references.bib} %Imports bibliography file

%\DefineBibliographyStrings{spanish}{%
%	andothers = {et al.},
%}

%Import the natbib package and sets a bibliography  and citation styles
\usepackage{natbib}
\bibliographystyle{abbrvnat}
\setcitestyle{authoryear,open={(},close={)}}



\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\lfoot{\varAuthor}
\cfoot{\varTitleDocument}
\rfoot{\thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{1pt}



%-------------------------------------
\usepackage[acronym, automake, toc]{glossaries}
\makeglossaries
\newacronym{meg}{MEG}{magnetoencefalografía}
\newacronym{eeg}{EEG}{electroencefalografía}
\newacronym{ieeg}{iEEG}{electroencefalografía intracraneal}
\newacronym{mri}{MRI}{imagen por resonancia}
\newacronym{squid}{SQUID}{dispositivos superconductores de interferencia cuántica }
\newacronym{fmri}{fMRI}{imagen por resonancia magnética funcional}
\newacronym{bold}{BOLD}{blood-oxygen-level dependent}
\newacronym{tms}{TMS}{estimulación magnética transcraneana}
\newacronym{rtms}{rTMS}{estimulación magnética transcraneana repetida}
\newacronym{tdcs}{TDCS}{estimulación transcraneal con corriente directa}
\newacronym{fda}{FDA}{Administración de Alimentos y Medicamentos, Estados Unidos}


\usepackage[pagebackref=true]{hyperref}
\hypersetup{
	pdftitle=\varTitleDocument,
	pdfauthor=\varAuthor,
	colorlinks=true,
	allcolors=black,
	citecolor=Blue4,
	urlcolor=magenta,
}

\renewcommand*\backref[1]{\ifx#1\relax \else (page #1) \fi}

\title{\varTitleDocument}
\author{Mario Michiels}
%Date is inserted here

%--------------------------------------------------------------
\begin{document}
\maketitle



%-----------------------------------------------------------
\begin{abstract}
Se presentan los fundamentos físicos de la generación de campos electromagnéticos neuronales, así como su detección mediante \acrfull{eeg} y \acrfull{meg}. Se explicará también cómo influyen los campos electromagnéticos del exterior sobre el cerebro, y las herramientas actuales como \acrfull{fmri} y \acrfull{tms}.

Se hará un enfoque teórico sobre posibles análisis a gran escala con machine learning de los datos obtenidos con las herramientas explicadas. Siguiendo esta metodología, se presenta NeuroSuite, un software que está siendo desarrollado por el autor de este trabajo para el departamento de Inteligencia Artificial de la Universidad Politécnica de Madrid. La plataforma integra múltiples herramientas neurocientíficas, alguna de ellas de especial utilidad en el estudio de las corrientes eléctricas neuronales.
\end{abstract}

\newpage

\tableofcontents

\newpage

%-----------------------------------------------------------
\section{Introducción teórica / contextualización}

%Section text (see \citep{Baillet2017} )
%\subsection{Subsection title}
%As you can see in the figure \ref{fig:mesh1}, there is a tiger jumping.
%Creating a footnote is easy.\footnote{An example footnote.}
%\begin{figure}[H]
%	\centering
%	\includegraphics[width=0.5\textwidth]{tiger}
%	\caption{Tiger jumping}
%	\label{fig:mesh1}
%\end{figure}
Las neuronas son la unidad funcional del cerebro, en el caso de los humanos se estima que hay 10\textsuperscript{9} neuronas \citep{Azevedo2009} y aproximadamente 10\textsuperscript{14} conexiones entre ellas. Dichas conexiones, llamadas sinapsis, liberan una pequeña corriente eléctrica como consecuencia de la conexión entre las neuronas. (ver figura \ref{fig:neuron_synapsis}).
\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{neuron_synapsis}
	\caption{Tipos de sinapsis. Figura extraída de \cite{Hamalainen1993}.}
	\label{fig:neuron_synapsis}
\end{figure}


Por otro lado, se ha comprobado cómo los campos electromagnéticos externos influyen sobre el cerebro y durante el siglo XX se crearon herramientas para hacer uso de ellos.

%-----------------------------------------------------------
\section{Material y métodos}
En esta sección se hará una revisión de la generación y detección de los campos electromagnéticos neuronales (mediante \acrshort{eeg} y \acrshort{meg}), así como la influencia de los campos electromagnéticos externos sobre el cerebro (mediante \acrshort{fmri} y \acrshort{tms}).

\subsection{\acrshort{eeg}}
\subsubsection{Fundamentos físicos}\label{subsec:eeg_fundamentos_fisicos}
Se estima que hay aproximádamente 10 neuronas de tipo piramidal por mm\textsuperscript{2} y miles de sinapsis por cada neurona \citep{Hamalainen1993}, cuya activación se produce en el orden de milisegundos. Se ha comprobado que muchos estímulos desencadenan la activación de un grupo numeroso de neuronas, por tanto los potenciales eléctricos ó magnéticos son sumados en caso de que las neuronas estén alineadas en la misma dirección.

La corriente eléctrica resultante de la sinapsis es producida por el paso de iones Na\textsuperscript{+} , k\textsuperscript{+}, y Cl entre las membranas de las neuronas (ver figura \ref{fig:neuron_synapsis}), con el fin de regular la concentración de iones entre el interior y el exterior de las células. La principal fuente de la corriente eléctrica por la activación de neuronas se produce en la fase de postsinapsis. Cuando las moléculas transmisoras (ver figura \ref{fig:neuron_synapsis}) llegan a la neurona postsináptica, la membrana es alterada y se produce el campo eléctrico y la corriente fluye por la neurona. Cabe destacar que la corriente eléctrica también viaja a lo largo del axón (prolongación de la neurona especializada en conducir el impulso), produciéndose así un potencial de acción (ver figura \ref{fig:neuron_electric_and_magnetic}). Sin embargo el potencial realmente destacable es el postsináptico, ya que la suma del flujo de las corrientes dura hasta decenas de milisegundos (así es tiempo suficiente para que se solapen corrientes de múltiples neuronas y se sume su potencial (figura \ref{fig:neurons_electric_currents}), mientras que el potencial de acción sólo dura 1 milisegundo \citep{Hamalainen1993}.


\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.5\textwidth}
 	\includegraphics[width=\textwidth]{neuron_electric_and_magnetic}
	\caption{Corriente eléctrica y sus potenciales en una neurona, así como su campo magnético inducido. Figura extraída de \cite{Baillet2017}.}
	\label{fig:neuron_electric_and_magnetic}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.4\textwidth}
 	\includegraphics[width=\textwidth]{neurons_electric_currents}
	\caption{Corriente eléctrica de múltiples neuronas. Se observa que el potencial postsináptico es más influyente, ya que dura hasta decenas de milisegundos y da tiempo suficiente a que se solapen y se sumen las señales de otras neuronas. Figura extraída de \cite{Baillet2017}.}
	\label{fig:neurons_electric_currents}
	\end{minipage}
\end{figure}
 
La fuerza de la corriente entre las neuronas disminuye según las conexiones sinápticas se alejan. La constante de la longitud de la disminución de esta fuerza se describe en la ecuación \ref{equ:potential_lenght}, donde $g_{m}$ es la conductividad de la membrana y $r_{s}$ es la resistencia del fluido intracelular por unidad de longitud. En el caso de una neurona cortical, el rango de $\lambda$ varía entre 0,1 y 0,2 mm.

\begin{equation}\label{equ:potential_lenght}
 \lambda=(g_{m}r_{s})^{-1/2} 
\end{equation}

\subsubsection{Detección}
El primer \acrshort{eeg} humano se realizó en 1929 \citep{Berger1929}. Dado que las corrientes eléctricas entre dos neuronas son de un orden de magnitud muy pequeño, para poder detectarlas se deben activar grupos de varios miles de neuronas al mismo tiempo. Del mismo modo, la posición de las neuronas influye de manera importante a la hora de detectar su activación. Así, se observa que las neuronas del córtex (ver figura \ref{fig:human_brain_parts}) son las que más contribuyen a la señal detectada, especialmente aquellas piramidales, ya que se agrupan en la misma dirección, por lo cúal se suma su campo eléctrico (ver figura \ref{fig:position_neurons_fields} C y figura \ref{fig:neurons_electric_currents})

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{human_brain_parts}
	\caption{Esquema en el que se ven algunas de las zonas más importantes del cerebro humano. Figura extraída de \cite{Hamalainen1993}.}
	\label{fig:human_brain_parts}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{position_neurons_fields}
	\caption{Organización espacial de las neuronas. Destaca la imagen C, donde se aprecia como las neuronas piramidales del córtex se alinean de manera paralela, una disposición que implica que se puedan sumar sus corrientes y su campo magnético. Figura extraída de \cite{Hansen2010}.}
	\label{fig:position_neurons_fields}
\end{figure}

El \acrshort{eeg} es una técnica no invasiva, por lo que las señales se atenúan porque deben atravesar diferentes tejidos hasta la superficie del cuero cabelludo. Esta atenuación es más fuerte en las señales eléctricas que en las magnéticas, ya que el líquido cefalorraquídeo, cráneo y cuero cabelludo presentan diferente conductividad eléctrica.

Para realizar un \acrshort{eeg} se colocan entre 10 y 128 electrodos en el cuero cabelludo del paciente (ver figura \ref{fig:eeg_cap}). Las diferencias de voltaje varían entre los 10$\mu$V y los 100$\mu$V, sin embargo existe otra manera de realizar el \acrshort{eeg} llamada \acrfull{ieeg}, en el cual se colocan los electrodos en el córtex mediante un procedimiento quirúrgico. Las medidas del \acrshort{eeg} se obtienen mediante la diferencia de potencial entre dos electrodos del cuero cabelludo o bien mediante la diferencia de potencial entre un electrodo del cuero cabelludo y un electrodo de referencia en el lóbulo de la oreja. 

La resolución temporal del \acrshort{eeg} es muy alta, lo cual permite equipararla al orden de magnitud en el que suceden las corrientes neuronales, es decir, en milisegundos, para el caso de la señal sería del orden de kHz. Las señales no tienen forma Gaussiana (ver figura \ref{fig:EEG_signals_example}), y se han establecido diferentes tipos de ondas (Beta: $>$13Hz; Alpha: 8-13 Hz; Theta: 4-8 Hz; Delta: 0,5-4 Hz), según su frecuencia y forma, para las cuales se han asociado diferentes estados y características (ver figura \ref{fig:eeg_waves_type}).

El potencial captado por los electrodos puede tener su origen en múltiples puntos del córtex, y es complicado saber con precisión cuál es exactamente la fuente del potencial. Por eso se dice que el \acrshort{eeg} presenta difícil resolución espacial, también conocido como el problema inverso. Este problema se puede solucionar en parte combinando el \acrshort{eeg} con \acrshort{fmri} (ver sección \ref{sec:fmri}).


\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\textwidth]{eeg_cap}
	\caption{Dispositivo \acrshort{eeg} de 128 canales por parte de la compañía Cognionics. Modelo Mobile-128. Figura extraída de \cite{Cognionics}.}
	\label{fig:eeg_cap}
\end{figure}

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.6\textwidth}
		\includegraphics[width=\textwidth]{EEG_signals_example}
		\caption{Detección de \acrshort{eeg} de 60 canales. Fue aplicado \acrshort{tms} al lado izquierdo, en el cual se ve un incremento de la actividad. Figura extraída de \cite{Kahkonen2005}.}
		\label{fig:EEG_signals_example}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.3\textwidth}
		\includegraphics[width=\textwidth]{eeg_waves_type}
		\caption{Señal \acrshort{eeg} en el que se ven diferentes tipos de onda. Figura extraída de \cite{Teplan2002}.}
		\label{fig:eeg_waves_type}
	\end{minipage}
\end{figure}

\subsection{\acrshort{meg}}
\subsubsection{Fundamentos físicos}
Por las ley de {\large  $\phi$}ersted se conoce que todo flujo eléctrico induce un campo magnético alrededor de él (comúnmente recordado como la regla de la mano derecha), por tanto se deduce que se generan campos magnéticos procedentes de la actividad neuronal. 

Hay que tener en cuenta que las corrientes ohmicas pasivas circundantes a la corriente primaria de las neuronas, también tienen importancia en cuanto a la generación de campos magnéticos.

Se puede calcular el campo magnético, así como el eléctrico por las leyes de Maxwell. Sin embargo dado que las señales del \acrshort{meg} varían relatívamente poco en el tiempo, los efectos de la inducción pueden ser obviados \citep{Hansen2010} y las leyes de Maxwell no es necesario tenerlas en cuenta, simplemente se aplican las leyes originales de Ohm, Ampère y Coulomb. Por tanto se puede simplificar la ecuación para calcular el campo magnético, así como el eléctrico (ecuación \ref{equ:magnetic_field_meg}, ver \cite{Hansen2010} para la demostración matemática), siendo $\nabla$ la cantidad de rotación del vector, $\overline{B}$ el campo magnético, $\mu$ la permeabilidad magnética, $\overline{J{i}}$ la corriente imprimida, $\sigma$ la conductividad del medio y $\overline{E}$ el campo eléctrico.

\begin{equation}\label{equ:magnetic_field_meg}
\nabla \times \overline{B} = \mu (\overline{J{i}} + \sigma \overline{E})
\end{equation}

\subsubsection{Detección}
La primera medición humana de \acrshort{meg} fue en 1972 \citep{Cohen1972}. Es una técnica no invasiva, que frente al \acrshort{eeg} (en el cual no importa tanto la disposición espacial), sólo se pueden detectar los campos magnéticos que están orientados de manera perpendicular al cráneo \citep{Ahlfors2010} (ver figura \ref{fig:meg_signal_cortex} y \ref{fig:cortex_fields}), por tanto las neuronas piramidales del córtex son las más destacables en las mediciones de \acrshort{meg} (ver figura \ref{fig:position_neurons_fields} C). Además, al igual que en el \acrshort{eeg}, es necesaria la activación de múltiples neuronas al mismo tiempo para que la fuerza de la señal sea suficiente como para recibirla. Para un \acrshort{meg} se estima que se deben activar al menos entre 10.000 y 50.000 neuronas para que se pueda recibir la señal en el exterior (ver figura \ref{fig:neuron_electric_and_magnetic}). El \acrshort{meg} se puede incluso combinar con el \acrshort{eeg}, sin embargo hay que tener en cuenta que las señales no son totalmente independientes (ver figura \ref{fig:EEG_and_fMRI_independence}).

La atenuación desde el origen de los campos electromagnéticos neuronales al exterior es menor que en el caso del \acrshort{eeg}, ya que los tejidos cerebrales hasta el cuero cabelludo tienen permeabilidad magnética constante. Sin embargo, la fuerza del campo magnético es extremadamente pequeña (ver figura \ref{fig:meg_signals_compared}), hasta el punto de que se tiene incluso que aislar el laboratorio que no haya interferencias externas. Por tal motivo no fue hasta la invención de los sensores magnetómetros llamados \acrfull{squid} \citep{Zimmerman1970} lo que permitió poder medir campos magnéticos con fuerzas tan pequeñas. Los actuales sensores magnetómetros presentan forma de casco y contienen más de 300 \acrshort{squid} en su interior (ver figura \ref{fig:meg_device_neuromag}). Además, para que el campo magnético no sea atenuado, el sensor contiene helio líquido (gran permeabilidad magnética) a -269 ºC \citep{Hari2012} (ver figura \ref{fig:MEG_device_scheme}).

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.6\textwidth}
	\includegraphics[width=\textwidth]{meg_signals_compared}
	\caption{Comparativa de los valores de los campos magnéticos. Figura extraída de \cite{Hamalainen1993}.}
	\label{fig:meg_signals_compared}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.3\textwidth}
	\includegraphics[width=\textwidth]{MEG_device_scheme}
	\caption{Esquema del interior de un dispositivo para medir \acrshort{meg}. Figura extraída de \cite{Hamalainen1993}.}
	\label{fig:MEG_device_scheme}
	\end{minipage}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{meg_device_neuromag}
	\caption{Dispositivo actual de \acrshort{meg} por parte de la compañía Elekta Neuromag. Figura extraída de \cite{ElektaNeuromag2018}.}
	\label{fig:meg_device_neuromag}
\end{figure}

La resolución temporal es muy alta (ver figura \ref{fig:meg_signal_example}), equiparable al \acrshort{eeg}, pero también se presenta el problema inverso, aunque igualmente se puede solucionar en parte combinando el \acrshort{meg} con \acrshort{fmri} (ver sección \ref{sec:fmri}).


\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{meg_signal_cortex}
	\caption{Recepción de campos electromagnéticos afectada por los pliegues del córtex. Figura extraída de \cite{Vrba2001}.}
	\label{fig:meg_signal_cortex}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{cortex_fields}
	\caption{Campos electromagnéticos afectados por los pliegues del córtex. Esquema más detallado de la figura \ref{fig:cortex_fields}. Figura extraída de \cite{Hansen2010}.}
	\label{fig:cortex_fields}
\end{figure}



\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{meg_signal_example}
	\caption{Ejemplo de una señal \acrshort{meg}. Figura extraída de \cite{Hansen2010}.}
	\label{fig:meg_signal_example}
\end{figure}

\subsection{\acrshort{fmri}}\label{sec:fmri}
\subsubsection{Fundamentos físicos y detección}
Para entender la \acrfull{fmri} \citep{Sandman1984} debemos entender primero los fundamentos físicos de la \acrfull{mri} \citep{Damadian1971}. La \acrshort{mri}, a diferencia del \acrshort{eeg} y \acrshort{meg}, no nos muestra información fucional sino estrcutural.
Esto es debido a que los átomos de hidrógeno presentan un protón (carga eléctrica positiva), lo cúal produce un giro aleatorio en el protón cuando no se aplica ningún campo magnético (ver figura \ref{fig:proton_spinning}). Al aplicar el intenso campo magnético del \acrshort{mri} (ver figuras \ref{fig:mri_device1} y \ref{fig:mri_device2}), la mayoría de los átomos de hidrógeno del paciente se reorientan hacia dicho campo (ver figura \ref{fig:protons_orient_magnetic}). Al mismo tiempo se aplican pulsos de radiofrecuencia, que provocan que algunos protones se reorienten en el sentido contrario. Posteriormente se dejan de enviar pulsos de radiofrecuencia, y los protones pasan a un estado de relajación, es decir, un tiempo en el que se vuelven a orientar en el sentido del campo magnético del \acrshort{mri}. El tiempo de relajación varía entre los tejidos, por ejemplo en el H\textsubscript{2}O el tiempo de relajación es relativamente largo. Este cambio de estado en la relajación de los protones, genera una energía que se libera en forma de radiofrecuencia y es captado por receptores de radiofrecuencia presentes en el escáner \acrshort{mri} (ver figura \ref{fig:mri_rf}). Por último, la señal de radiofrecuencia es convertida a una señal digital, y al aplicar transformadas de Fourier obtenemos las imágenes.

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.5\textwidth}
		\includegraphics[width=\textwidth]{mri_device1}
		\caption{Esquema del interior de un dispositivo de \acrshort{mri}. Figura extraída de  \citep{Schild1990}.}
		\label{fig:mri_device1}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{mri_device2}
		\caption{Dispositivo \acrshort{mri}. Figura extraída de \cite{Blink2004}.}
		\label{fig:mri_device2}
	\end{minipage}
\end{figure}

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.3\textwidth}
		\includegraphics[width=\textwidth]{proton_spinning}
		\caption{Protón girando por su carga positiva. Figura extraída de  \citep{Schild1990}.}
		\label{fig:proton_spinning}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{protons_orient_magnetic}
		\caption{Protones orientándose por la influencia de un campo magnético. Figura extraída de \cite{Blink2004}.}
		\label{fig:protons_orient_magnetic}
	\end{minipage}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{mri_rf}
	\caption{Excitación de los protones mediante pulsos de radiofrecuencia y su consecuente relajación y emisión de radiofrecuencia, que recibe el escáner \acrshort{mri}. Figura extraída de \cite{Blink2004}.}
	\label{fig:mri_rf}.
\end{figure}

La importancia de la \acrshort{fmri} frente a la \acrshort{mri} radica en la capacidad de poder captar no sólo una imagen estructural sino también funcional. Esto se se basa en un efecto llamado \acrfull{bold}. La activación neuronal provoca un incremento en el flujo sanguíneo y en la oxigenación (ver figura \ref{fig:bold_effect_blood}).

La sangre oxigenada es diamagnética, mientras que la sangre desoxigenada es paramagnética, lo que la hace ser atraída por el campo magnético del \acrshort{mri}. Ese componente paramagnético en la sangre desoxigenada, es gracias a la desoxihemoglobina, que es atraída por el campo magnético del \acrshort{mri}. Cuando esto sucede, la desoxihemoglobina comienza a tener un desfase de señal y por tanto se introducen distorsiones en el campo magnético. Así, en áreas de mayor activación neuronal, se introduce más sangre oxigenada, que al ser diamagnética disimula la influencia paramagnética de la desoxihemoglobina, cuyo desfase queda disimulado, así como su distorsión en el campo magnético. Debido a la reducción de distorsiones, se recibe un incremento de señal en el escáner \acrshort{mri}, lo cual permite conocer que en ese área hay un incremento de activación neuronal.


\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{bold_effect_blood}
	\caption{Efecto \acrshort{bold}. Aumento de flujo sanguíneo y oxigenación en la activación neuronal. Figura extraída de \cite{Dogil2002}.}
	\label{fig:bold_effect_blood}
\end{figure}


\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.3\textwidth}
		\includegraphics[width=\textwidth]{mri_example_image}
		\caption{Ejemplo de imagen tomada por \acrshort{mri}. Figura extraída de \cite{Blink2004}.}
		\label{fig:mri_example_image}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.6\textwidth}
		\includegraphics[width=\textwidth]{fmri_image_bold}
		\caption{Ejemplo de imagen tomada por \acrshort{fmri}. Se aprecia también la intensidad de la señal \acrshort{bold}. Figura extraída de \cite{Mao2002}.}
		\label{fig:fmri_image_bold}
	\end{minipage}
\end{figure}


Dentro del córtex hay muchas señales neuronales a diferentes frecuencias de oscilación, por eso no se conoce relación exacta entre la actividad neuronal y las señales hemodinámicas, sin embargo sí hay evidencias de fuerte correlacción entre los patrones espaciales de la hemodinámica y la actividad eléctrica neuronal \citep{Benson1996} (ver figura \ref{fig:neuronal_techniques_factors}). Por este último motivo, el \acrshort{fmri} resulta especialmente útil al combinarlo con técnicas como el \acrshort{eeg} (ver figura \ref{fig:EEG_and_fMRI_independence}) y/o \acrshort{meg}.
 
A diferencia del \acrshort{eeg} y \acrshort{meg}, la \acrshort{fmri} presentan una alta resolución espacial, entre 1 y 4 mm de resolución en el plano. Sin embargo, la resolución temporal es más baja que en los métodos anteriores, es decir, entre 0,1 y 1 segundos (del orden de 1 a 10 Hz).

\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{neuronal_techniques_factors}
	\caption{Relaciones causales no conocidas con exactitud para las sañeles \acrshort{bold} en \acrshort{fmri}. Figura extraída de \cite{Singh2012}.}
	\label{fig:neuronal_techniques_factors}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{EEG_and_fMRI_independence}
	\caption{Relaciones de independencia entre el \acrshort{eeg}, \acrshort{meg} y \acrshort{fmri}. Figura extraída de \cite{Malmivuo2012}.}
	\label{fig:EEG_and_fMRI_independence}
\end{figure}


\subsection{\acrshort{tms}}
\subsubsection{Fundamentos físicos}
Por último, es importante entender cómo afecta la inducción electromagnética externa sobre el cerebro, o dicho de otro modo, cómo afecta un campo magnético que penetra hasta el cerebro e induce una corriente eléctrica en su interior. Esta teoría de la inducción electromagnética fue descrita por Michael Faraday en 1831, sin embargo no fue hasta 1985 cuando \cite{Barker1985} consiguieron aplicarlo sobre el cerebro.

La \acrshort{tms} es una técnica no invasiva y la ley de la inducción magnética se le aplica de la siguiente manera: se coloca sobre el cuero cabelludo un dispositivo (ver figura \ref{fig:tms_device_human}) que genera pulsos de alta corriente eléctrica en la bobina que contienen. Dicha corriente induce un campo magnético perpendicular a la bobina (figura \ref{fig:tms_induction}), que llega hasta el córtex. Siguiendo la ley de la inducción magnética, cuando una corriente eléctrica es activada por una espiral (el cable del dispositivo en este caso), se produce un campo magnético y en el caso de encontrarse cerca una segunda estructura que actúe como espiral o bobina (en este caso el córtex) se induce una corriente eléctrica (ver figura \ref{fig:tms_scheme}). El pulso generado por el dispositivo debe ser lo más breve posible, así el cambio en el gradiente magnético será brusco y se provocará la inducción eléctrica en el córtex. La brevedad de los pulsos explica por qué este fenómeno de inducción de corriente eléctrica no se produce en técnicas como la \acrshort{mri}, ya que en ese caso el campo magnético provocado es constante.

\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{tms_device_human}
		\caption{Dispositivo \acrshort{tms} por parte de la compañía Neurosoft, modelo Neuro-MS/D Therapeutic. Figura extraída de \cite{Neurosoft2018}.}
		\label{fig:tms_device_human}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{tms_induction}
		\caption{Campo magnético inducido por la corriente producida en el dispositivo del \acrshort{tms}. Figura extraída de \cite{Hallett2000}.}
		\label{fig:tms_induction}
	\end{minipage}
\end{figure}


\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\textwidth]{tms_scheme}
	\caption{Esquema del campo magnético de la \acrshort{tms} induciendo corriente eléctrica en el córtex. Figura extraída de \cite{Ruohonen2010}.}
	\label{fig:tms_scheme}
\end{figure}

Las consecuencias de la corriente eléctrica generada en el córtex es la despolarización de las membranas de las neuronas (ver sección \ref{subsec:eeg_fundamentos_fisicos}). Si la despolarización sobrepasa cierto nivel, la neurona se llegará a descargar y se producirá el potencial de acción por el axón (ver figura \ref{fig:neuron_electric_and_magnetic}). Esta activación de las neuronas puede incluso propagar conexiones sinápticas con las neuronas adyacentes y subyacentes. Estudios recientes describen también que como consecuencia de estimular ciertas neuronas, se puede producir una inhibición de las dendritas de las neuronas subyacentes, lo cúal inhibiría la activación de dichas neuronas  \citep{Murphy2016} (en el tercer tipo de sinapsis de la figura \ref{fig:neuron_synapsis} se aprecia el mecanismo de inhibición de las neuronas).

También es importante saber que existen técnicas similares como la \acrfull{tdcs}, la cual se basa en producir directamente la corriente eléctrica al córtex, mediante electrodos en el cuero cabelludo. Sin embargo esta técnica tiene menos influencia en la activación de las neuronas, ya que se realiza con corrientes muy pequeñas (entre 1 y 2 mA \citep{MorenoDuarte2014}) con el fin de no activar el área del córtex encargada de sentir el dolor. Por tanto no es suficiente para causar una despolarización tan fuerte de las membranas neuronales que produzca el potencial de acción de la neuronas, así como tampoco es capaz de poder propagar esa corriente a las neuronas subyacentes.

La resolución temporal de la \acrshort{tms} no es muy alta puesto que la corriente eléctrica del dispositivo se produce mediante pulsos muy breves, sin embargo mediante la repetición de rápidos pulsos, puede ser alrededor entre 5 y 20 Hz \citep{Lefaucheur2014a}. En cuanto a la resolución espacial, tampoco es muy alta, el campo magnético, que posteriormente induce la corriente eléctrica, atraviesa hasta 2 o 3 cm de profundidad del córtex. Cabe destacar también que si se cambia la orientación del dispositivo sobre el cuero cabelludo, como se puede deducir, varía el área de acción de la corriente eléctrica inducida.

\subsubsection{Usos clínicos}
La \acrshort{tms} se ha utilizado para investigación en la activación neuronal. Desde el punto de vista físico se puede entender como el método contrario al \acrshort{meg}, ya que este induce campos magnéticos a partir de la corriente eléctrica, y la \acrshort{tms} induce corriente eléctrica a partir del campo magnético. Se ha estudiado también el uso de \acrshort{eeg} de manera simultánea a la \acrshort{tms} (ver figura \ref{fig:EEG_signals_example}), para monitorizar las corrientes eléctricas generadas. Por ejemplo, para estudiar el córtex motor se ha investigado que la activación mediante \acrshort{tms} puede provocar respuestas motoras en determinadas zonas (ver figura \ref{fig:tms_stimuli_finger}).

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{tms_stimuli_finger}
	\caption{Estímulo por \acrshort{tms} en el córtex motor, que produce movimiento en el dedo índice. Figura extraída de \cite{Aziz-Zadeh2002}.}
	\label{fig:tms_stimuli_finger}
\end{figure}

A diferencia de los métodos anteriores, una aplicación importante de está técnica consiste en su uso como terapia de trastornos neuropsiquiátricos, tanto es así que está oficialmente aprobado por la \acrfull{fda}. Un ejemplo en el que se puede apreciar claramente su uso como terapia es en el tratamiento de la depresión, ya que es un trastorno en el cúal se presenta asimetría frontal en el córtex. De tal manera se estudia estimular el lado menos influyente en la depresión e inhibir el lado más influyente. El fundamento del funcionamiento de estas técnicas se basa también en la plasticidad neuronal, es decir, que incluso después de haber recibido este tratamiento, esas zonas del córtex puedan tener actividad similar a la inducida durante el tratamiento. Sin embargo para ese efecto se debe hacer \acrfull{rtms}, es decir varias sesiones de \acrshort{tms}. Para ver más ejemplos sobre tratamientos con \acrshort{tms} y su eficacia, revisar \cite{Lefaucheur2014a}.

%-----------------------------------------------------------
\section{Planteamiento del problema}
Hasta estos últimos años, el tratamiento de los datos obtenidos se ha realizado mediante especialistas en la materia, de manera personal aunque ayudados en la medida de lo posible por las técnicas informáticas. Sin embargo, hasta ahora no habíamos tenido tanto poder computacional como para poder analizar de manera masiva toda la cantidad de datos obtenidos. La única manera de hacer estudios a tan gran escala es mediante las nuevas tecnologías informáticas que han surgido estos últimos años, ya que de manera humana sería algo completamente imposible.

\subsection{Machine learning}
El machine learning es un área de la inteligencia artificial que se centra en analizar los datos mediante algoritmos con el fin de obtener información útil sobre ellos. Por ejemplo, en nuestro grupo de investigación en la Universidad Politécnica de Madrid, estamos utilizando redes Bayesianas para obtener relaciones de dependencia entre variables morfológicas de las neuronas \citep{Bielza2014}. Una aplicación más extensible aún, en la cual también trabajamos, consiste en poder observar si modificando los valores de las medidas morfológicas de las neuronas, se producirían cambios en su electrofisiología. Nótese que todos estos estudios se realizan desde el punto de vista computacional. Para poder tener verdadera validez, una vez que se han creado los modelos, deben consultarse con especialistas en el campo, en este caso neurocientíficos, para ver si los modelos son coherentes y proporcionan información novedosa.

La ejecución de estos algoritmos, cuando los datos son masivos, sólo es posible en la actualidad. Por ejemplo, para representar las relaciones entre variables obtenidas de las redes bayesianas, en nuestro grupo nos estamos planteando utilizar alguna solución a gran escala como BeGraph \citep{NextLimit2018} (ver figura \ref{fig:begraph}). 

\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{begraph}
	\caption{Ejemplo de red creada con BeGraph \citep{NextLimit2018}.}
	\label{fig:begraph}
\end{figure}

%-----------------------------------------------------------
\section{Resultados / aplicaciones}
\subsection{NeuroSuite}
El autor de este trabajo está desarrollando el software NeuroSuite  \citep{Michiels2018} (futuro artículo planeado), una plataforma capaz de ejecutar múltiples herramientas neurocientíficas completamente online, sin necesidad de instalar nada en el ordenador del usuario.

En el ámbito de este trabajo, el  electromagnetismo, herramientas ya incluidas como el análisis de la morfología de neuronas (ver figura \ref{fig:neurosuite_l_measure}), reparación virtual de dendritas en 3D, o generación virtual de espinas de las dendritas (ver figura \ref{fig:neurosuite_3dspine}), pueden ser de utilidad para estudios sobre cómo afecta la morfología para la transmisión y generación de las corrientes eléctricas entre las neuronas.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{neurosuite_l_measure}
	\caption{Software L-Measure \citep{Scorcioni2008} midiendo variables morfológicas de neuronas de manera online desde la plataforma NeuroSuite.}
	\label{fig:neurosuite_l_measure}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{neurosuite_3dspine}
	\caption{Software 3DSpineS \citep{Luengo} creando espinas virtuales de dendritas, ejecutándose desde la plataforma NeuroSuite.}
	\label{fig:neurosuite_3dspine}
\end{figure}

\subsubsection{MultiMap}
El autor de este trabajo está desarrollando la versión online de
\href{http://neurosuite.sytes.net/micro/multimap}{MultiMap} (ver figura \ref{fig:neurosuite_fmri}), una herramienta para visualización y análisis de imágenes, creada originalmente para escritorio por nuestro compañero \cite{GherardoVarando}. La herramienta también se incluye en la plataforma NeuroSuite.

Los usuarios de la aplicación pueden subir sus propias imágenes, por ejemplo imágenes de muy alta resolución de las resultantes de una \acrshort{mri} o \acrshort{fmri}. Una vez subida la imagen, nuestro servidor procederá a crear un mapa a partir de ella (dividiendo la imagen en segmentos) para poder ser visualizada de manera eficiente (algo similar al procesamiento que hace Google Maps para que podamos visualizar su mapa de manera eficiente). También se presentará la funcionalidad de analizar las imágenes mediante diversos algoritmos  (por ejemplo detección automática de puntos), así como la posibilidad de hacer anotaciones sobre la imagen, añadir nuevas capas, compartir el mapa, etc.

\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{neurosuite_fmri}
	\caption{Imagen de una \acrshort{fmri} vista en la aplicación online de MultiMap ejecutándose en la plataforma NeuroSuite.}
	\label{fig:neurosuite_fmri}
\end{figure}


%-----------------------------------------------------------
\section{Discusión}
Gracias a las nuevas tecnologías informáticas, ahora se le puede sacar aún más utilidad a técnicas inventadas hace relativamente bastante tiempo pero que hasta ahora nunca se pudieron explotar a una escala tan masiva. Por otro lado, aunque en la revisión de las técnicas expuestas se ha omitido, cabe destacar que hay numerosos avances físicos para crear nuevas técnicas y mejorar las técnicas descritas. Por ejemplo se está investigando mejorar la \acrshort{tms} para alcanzar mayor profundidad en su efecto.

Estamos en una época de plena expansión tanto en el sentido físico y biológico, como informático. Por tanto ahora más que nunca se necesita una colaboración interdisciplinar.

\section{Agradecimientos}
En referencia al desarrollo de la plataforma NeuroSuite, se añaden los siguientes agradecimientos: \\

\textit{This work has been partially supported by the Spanish Ministry of Economy, Industry and Competitiveness through the Cajal Blue Brain (C080020-09; the Spanish partner of the Blue Brain initiative from EPFL) and TIN2016-79684-P projects, and by the Regional Government of Madrid through the S2013/ICE-2845-CASI-CAM-CM project. This project has received funding from the European Union`s Horizon 2020 Research and Innovation Programme under Grant Agreement No. 720270 (HBP SGA1).}



\clearpage
\printglossary
\printglossary[type=\acronymtype]
\bibliography{references}

\end{document}
