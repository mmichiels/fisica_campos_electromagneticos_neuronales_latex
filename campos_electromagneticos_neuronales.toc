\select@language {spanish}
\contentsline {section}{\numberline {1}Introducci\IeC {\'o}n te\IeC {\'o}rica / contextualizaci\IeC {\'o}n}{3}{section.1}
\contentsline {section}{\numberline {2}Material y m\IeC {\'e}todos}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}\acrshort {eeg}}{4}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Fundamentos f\IeC {\'\i }sicos}{4}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Detecci\IeC {\'o}n}{6}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2}\acrshort {meg}}{8}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Fundamentos f\IeC {\'\i }sicos}{8}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Detecci\IeC {\'o}n}{9}{subsubsection.2.2.2}
\contentsline {subsection}{\numberline {2.3}\acrshort {fmri}}{12}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Fundamentos f\IeC {\'\i }sicos y detecci\IeC {\'o}n}{12}{subsubsection.2.3.1}
\contentsline {subsection}{\numberline {2.4}\acrshort {tms}}{17}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}Fundamentos f\IeC {\'\i }sicos}{17}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}Usos cl\IeC {\'\i }nicos}{19}{subsubsection.2.4.2}
\contentsline {section}{\numberline {3}Planteamiento del problema}{20}{section.3}
\contentsline {subsection}{\numberline {3.1}Machine learning}{20}{subsection.3.1}
\contentsline {section}{\numberline {4}Resultados / aplicaciones}{21}{section.4}
\contentsline {subsection}{\numberline {4.1}NeuroSuite}{21}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}MultiMap}{23}{subsubsection.4.1.1}
\contentsline {section}{\numberline {5}Discusi\IeC {\'o}n}{24}{section.5}
\contentsline {section}{\numberline {6}Agradecimientos}{24}{section.6}
\contentsline {section}{Siglas}{25}{section*.2}
\contentsline {section}{Referencias}{25}{section*.4}
